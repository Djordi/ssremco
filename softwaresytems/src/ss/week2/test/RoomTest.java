package ss.week2.test;

import org.junit.Before;
import static org.junit.Assert.*;

import org.junit.Test;
import ss.week2.hotel.Guest;
import ss.week2.hotel.Room;
import ss.week2.hotel.*;

import static org.junit.Assert.assertEquals;

public class RoomTest {
	private Guest guest;
	private Room room;
	private Safe safe;

	@Before
	public void setUp() {
		guest = new Guest("Jip");
		room = new Room(101);
		safe = new Safe();
	}

	@Test
	public void testSetUp() {
		assertEquals(101, room.getNumber());
	}

	@Test
	public void testSetGuest() {
		room.setGuest(guest);
		assertEquals(guest, room.getGuest());
	}
	
	@Test
    public void testSafe() {
    	assertNotNull(room.getSafe());
    	room.getSafe().activate("hotels");
    	assertTrue(room.getSafe().isActive());
    	room.getSafe().deactivate();
    	assertFalse(room.getSafe().isActive());
    	room.getSafe().activate("hotels");
    	room.getSafe().open("hotels");
    	assertTrue(room.getSafe().isOpen());
    }

}
