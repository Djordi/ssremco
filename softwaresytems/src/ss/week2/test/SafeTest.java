package ss.week2.test;

import static org.junit.Assert.*;


import ss.week2.hotel.*;

import org.junit.Before;
import org.junit.Test;

public class SafeTest {

	private Safe a;
	private Safe b;
	private Safe c;

	@Before
	public void setUp() throws Exception {
		a = new Safe();
		b = new Safe();
		c = new Safe();

	}

	@Test
	public void testCorrect() {
		// test whether everything works when right stuff is entered
		a.activate("wachtwoord");
		assertTrue(a.isActive());
		a.open("wachtwoord");
		assertTrue(a.isOpen());
		a.close();
		assertFalse(a.isOpen());
		a.deactivate();
		assertFalse(a.isActive());
		assertNotNull(a.getPassword());
	}

	@Test
	public void testWrongPassword() {
		// test whether everything works when wrong password is entered
		b.activate("wachtwoord");
		assertTrue(b.isActive());
		b.open("fout");
		assertFalse(b.isOpen());
		b.deactivate();
		assertFalse(b.isActive());
		
	}

	@Test
	public void testWrongActivationkey() {
		// test whether everything works when wrong activationkey is entered
		c.activate("fout");
		assertFalse(c.isActive());
		c.open("wachtwoord");
		assertFalse(c.isOpen());
		

	}

}
