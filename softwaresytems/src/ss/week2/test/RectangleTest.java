package ss.week2.test;

import org.junit.Before;

import org.junit.Test;
import ss.week2.Rectangle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RectangleTest {
	
	private Rectangle r;
	private Rectangle g; 
	
	@Before
	public void setUp() { 
		r = new Rectangle(10,2);
		g = new Rectangle(4,5);
	}
	
	@Test
	public void testInitialcondition() {
		assertEquals(10, r.length());
		assertEquals(2, r.width());
		assertEquals(4, g.length());
		assertEquals(5, g.width());
	}
	
	@Test
	public void testCallculations() {
		assertEquals(20, r.area());
		assertEquals(24, r.perimeter());
		assertEquals(20, g.area());
		assertEquals(18, g.perimeter());
		
		
		
	}
	
	

}
