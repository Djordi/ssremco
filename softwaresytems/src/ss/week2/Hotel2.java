package ss.week2;

import ss.week2.hotel.Guest;
import ss.week2.hotel.Password;
import ss.week2.hotel.Room;

public class Hotel2 {
	
	//-------Instance Variables-------
	public String hname;
	public Room room1;
	public Room room2;
	public Password password;
	
	//-------Constructor--------------
	public Hotel(String s) {
		hname = s;
		room1 = new Room(101);
		room2 = new Room(102);
	
		
	}
	
	//-------Commands-----------------
	// Checks in a guest in a free room
	public Room checkIn(String pw, String name) {
		if (!(getPassword().testWord(pw))) {
			assert password.getPassword() != pw;
			return null;
		}
		if (getFreeRoom() == null) {
			assert room1.getGuest() != null;
			assert room2.getGuest() != null;
			assert getFreeRoom() == null;
			return null;
		}
		if (room1.getGuest() == null && room2.getGuest() == null) {
			room1.setGuest(new Guest(name));
			assert room1.getGuest().getName() == name;
			assert room2.getGuest() == null;
			return room1;
		}
		if (room1.getGuest() == null && room2.getGuest().getName() != name) {
			room1.setGuest(new Guest(name));
			assert room1.getGuest().getName() == name;
			assert room2.getGuest().getName() != name;
			return room1;
		}
		if (room1.getGuest().getName() != name) {
			room2.setGuest(new Guest(name));
			assert room2.getGuest().getName() == name;
			assert room1.getGuest().getName() != name;
			return room2;
		} else {
			return null;
		}
	}
	
	// checks out a guest from the room that it is in
	// if the guest is not in one of the rooms nothing happens
	public void checkOut(String name) {
		if (room1.getGuest() != null && room1.getGuest().getName() == name) {
			room1.setGuest(null);
			assert room1.getGuest() == null;
		}
		if (room2.getGuest() != null && room2.getGuest().getName() == name) {
			room2.setGuest(null);
			assert room2.getGuest() == null;
		}
	}

	//-------Queries------------------
	// returns a free room, if there is no room available return null
	public Room getFreeRoom() {
		if (room1.getGuest() == null) {
			assert room1.getGuest() == null;
			return room1;
		} else if (room2.getGuest() == null) {
			assert room2.getGuest() == null;
			return room2;
		} else {
			assert room1.getGuest() != null;
			assert room2.getGuest() != null;
			return null;
		}
	}
	
	// returns the room of the name of the guest given, if the guest is not in a room returns null
	public Room getRoom(String name) {
		if (room1.getGuest() != null && room1.getGuest().getName() == name) {
			assert room1.getGuest().getName() == name;
			return room1;
		} else if (room2.getGuest() != null && room2.getGuest().getName() == name) {
			assert room2.getGuest().getName() == name;
			return room2;
		} else {
			return null;
		}
	}
	
	// returns the password of the room that it is called on
	public Password getPassword() {
		return password;
	}
	
	// returns the name of the guest in room 1
	public String toString() {
		assert room1.getGuest() != null;
		return "Room 1: " + room1.getGuest().getName();
	}
}