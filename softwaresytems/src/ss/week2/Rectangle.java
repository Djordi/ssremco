package ss.week2;

public class Rectangle {
    private int length;
    private int width;

    //@ private invariant length >= 0 ;
    //@ private invariant width >= 0; 
    /**
     * Create a new Rectangle with the specified length and width.
     */
    /*@ 
  		requires theLength >= 0 && theWidth >= 0;
     	ensures this.length() == theLength;
     	ensures this.width() == theWidth;
     */
    public Rectangle(int theLength, int theWidth) {
    	assert this.length >= 0;
    	assert this.width >= 0;
    	this.length = theLength;
    	this.width = theWidth;
    }
    
    
    /**
     * The length of this Rectangle.
     */
    //@ ensures \result >= 0; 
    /*@ pure */ 
    public int length() {
    	assert this.length >= 0;
    	return this.length;
    }

    /**
     * The width of this Rectangle.
     */
    //@ ensures \result >= 0;
    /*@ pure */ 
    public int width() {
    	assert this.width >= 0;
    	return this.width;
    }

    /**
     * The area of this Rectangle.
     */
    //@ ensures \result >= 0;
    //@ ensures \result == length() * width();
    public int area() {
    	assert this.length >= 0 && this.width >= 0 && this.length * this.width >= 0;
    	return this.length * this.width;
    }

    /**
     * The perimeter of this Rectangle.
     */
    //@ ensures \result >=0;
    //@ ensures \result == 2 * (length() + width());
    public int perimeter() {
    	assert this.length >= 0 && this.width >= 0 && 2 * (this.length * this.width) >= 0;
    	return 2 * (this.length + this.width);
    }
}
