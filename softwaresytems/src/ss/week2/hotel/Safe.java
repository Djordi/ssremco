package ss.week2.hotel;

import java.util.Arrays;

public class Safe {

	// @ private invariant password == "";
	// @ private invariant active == boolean;
	// @ private invariant open == boolean;

	private Boolean active;
	private Boolean open;
	Password password;
	
	/*
	 * @ requires n >= 0 && activationkey == "" && password == "" && active ==
	 * false && open == false; ensures this.number == n; ensures
	 * this.activationkey == a; ensures this.password == p; ensures this.active
	 * == false; ensures this.open == false;
	 */

	public Safe() {
		password = new Password();
		active = false;
		open = false;
	}

	// activates the save when password is correct
	/*
	 * @ requires activationcode == activationkey; ensures this.active == true;
	 */

	public void activate(String activationcode) {
		
		if (password.acceptable(activationcode) == true) {
			this.active = true;
			assert this.active == true;
		}
	}

	// closes the save and deactivates it
	/*
	 * @ ensures this.active == false; ensures this.open == false;
	 */
	public void deactivate() {
		
		this.active = false;
		this.open = false;
		assert this.active == false;
		assert this.open == false;
	}

	// opens the safe if active and password is correct
	/*
	 * @ requires passwordcode == password; ensures this.open == true;
	 */
	public void open(String passwordcode) {
		if (this.active && password.testWord(passwordcode)) {
			this.open = true;
			assert this.open == true;
		}
	}

	// closes the safe
	/*
	 * @ ensures this.open == false;
	 */
	public void close() {
		this.open = false;
		assert this.open == false;
	}

	// indicates whether the safe is active
	/*
	 * @ ensures \result == boolean;
	 */
	/* @ pure */ public boolean isActive() {
		return this.active;

	}

	// indicates whether the safe is open
	/*
	 * @ ensures \result == boolean;
	 */
	/* @ pure */ public boolean isOpen() {
		return this.open;

	}

	// returns the password
	/*
	 * @ ensures \result == password;
	 */
	/* @ pure */ public Password getPassword() {
		return this.password;
	}
	
	
	public static void main(String [ ] args) {
		Safe safe;
		safe = new Safe();
		System.out.println(safe.getPassword());
		
	}

}
