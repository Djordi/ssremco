package ss.week2.hotel;

/**
 * Hotel room with number and possibly a guest.
 * @author Remco Loof
 * @version version 1.0
 */


public class Guest extends java.lang.Object {
	
	// ------------------ Instance variables ----------------

	private String name;
	public Room room;
	
	// ------------------ Constructor ------------------------

	/**
     * Creates a <code>Guest</code> with the given name and without Room
     * @param n name of the <code>Guest</code>
     */
	
	public Guest(String n){
		name = n;
		room = null;
	}
	
	// ------------------ Queries ------------------------
	
	/**
	 * Gives the name of the <code>Guest</code>
	 * @return the name of this <code>Guest</code>
	 */
	
	public String getName(){
		return this.name;
	}
	
	/**
	 * Gives the room of the <code>Guest</code>
	 * @return the room of this <code>Guest</code> 
	 */
	
	public Room getRoom() {
		return this.room;
	}
	
	public String toString(){
		return "Guest: " + this.name + "Room: " + this.room;
		//fill in
	}
	
	// ------------------ Commands ------------------------	
	
	/**
	 * Checks in a <code>Guest</code>
	 * @param r Room to be rented to this <code>Guest</code>
	 * @return true if checkin succeeded; false if this <code>Guest</code> already had a <code>Room</code>, or r already had a <code>Guest</code>
	 */
	
	public void checkin(Room r){
		if (this.room == null && r.getGuest() == null) {
			r.setGuest(this);
			this.room = r;
		
		}
		else {
		
		}
	
	}
	
	/**
	 * Checks out a <code>Guest</code>
	 * @return true if this action succeeded; false if Guest does not have a Room when this method is called
	 */
	
	public boolean checkout(){
		if (this.room != null) {
			this.room.setGuest(null);
			this.room = null;
			return true;
		}
		else {
			return false;
		}
		
	}
	
}
