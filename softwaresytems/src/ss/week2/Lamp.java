package ss.week2;

public class Lamp {
	
	
	//@ public invariant OFF == 0;
	//@ public invariant LOW == 1;
	//@ public invariant MED == 2; 
	//@ public invariant HIGH ==3; 
	
	public enum Setting{OFF, LOW, MED, HIGH}
	
	Setting setting;
	
	public Lamp(Setting setting) {
		this.setting = Setting.OFF;
	}
	
	public void switchSetting() { 
		switch (setting) {
		case OFF:
			setting = Setting.LOW;
			break;
		case LOW: 
			setting = Setting.MED;
			break;
		case MED: 
			setting = Setting.HIGH;
			break;
		case HIGH:
			setting = Setting.LOW;
			break;
		}
	}
}		