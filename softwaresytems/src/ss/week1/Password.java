package ss.week1;

public class Password extends java.lang.Object {
	
	public String password;
	public static final String INITIAL = ""; 
	
	public Password(){
		password = INITIAL;
	}
		
	public boolean acceptable(String suggestion) {
		if (suggestion.length() < 6 || suggestion.contains(" ")) {
			return false;
		}
		else {
			return true;
		}
		
	}
	
	public boolean testWord(String test) {
		if (test.equals(password)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean setWord(String oldpass, String newpass) {
		if (testWord(oldpass) && acceptable(newpass)) {
			password = newpass;
			return true;
		}
		else {
			return false;
		}
		
	}

}
