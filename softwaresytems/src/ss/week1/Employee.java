package ss.week1;

public class Employee {
	
	private int hours;
	private double rate;
	
	public double pay(int hours, double rate) {
		double check = 0;
		if (hours < 40) {
			check = hours * rate;
		}
		else {
			check = ((hours - 40) * (1.5 * rate)) + (40 * rate);
		}
		return check;
	
	}

}
