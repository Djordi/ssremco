package ss.week1;

public class Lamp {
	
	public final static int OFF = 0;
	public final static int LOW = 1;
	public final static int MED = 2;
	public final static int HIGH = 3;
	
	private int setting;
	
	public Lamp(int setting) {
		this.setting = setting;
	}
	
	public int getSetting() {
		return setting;	
	}
	
	public int switchSetting() { 
		if ((this.getSetting() + 1) < 4) {
			setting = this.getSetting() + 1;
		}
		else {
			setting = 0;			
		}
		return setting;
	}

}
