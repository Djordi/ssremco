package ss.week3;

public class Format {
	
	static void printLine(String text, double amount){
		System.out.printf("%-15s %-15s\n", text, amount); 
	}

	public static void main(String[] args) {
		printLine("text1", 1.00);
		printLine("other text", -12.12);
		printLine("something", 0.20);
	}
}
