package ss.week3.hotel;

public class Hotel {
	
	//Instance Variables
	private String name;
	private Room room1;
	private Room room2;
	
	//Constructor
	public Hotel(String n) {
		name = n;
		room1 = new Room(1);
		room2 = new Room(2);
	}
	
	//Commands
	//Checks in a guest in a free room
	public Room checkIn(String p, String n) {
		if (!(room1.getSafe().getPassword().testWord(p)) || getFreeRoom() == null) {
			return null;
		}
		else if (room1.getGuest() == null && room2.getGuest() == null) {
			room1.getSafe().activate(p);
			room1.setGuest(new Guest(n));
			assert room1.getGuest().getName() == n;
			assert room2.getGuest() == null;
			return room1;
		}
		else if (room1.getGuest() == null && (room2.getGuest().getName() != n)){
			room1.getSafe().activate(p);
			room1.setGuest(new Guest(n));
			assert room1.getGuest().getName() == n;
			assert room2.getGuest().getName() != n;
			return room1;
		}
		else if (room2.getGuest() == null && (room1.getGuest().getName() != n)) {
			room2.getSafe().activate(p);
			room2.setGuest(new Guest(n));
			assert room2.getGuest().getName() == n;
			assert room1.getGuest().getName() != n;
			return room2;
		}
		else { 
			return null;
		}
		
	}
	
	//Checks out a guest out of his room
	public void checkOut(String n) {
		if (!(room1.getGuest() == null) && room1.getGuest().getName() == n) {
			room1.getSafe().deactivate();
			room1.setGuest(null);
			assert room1.getGuest() == null;
		}
		else if (!(room2.getGuest() == null) && room2.getGuest().getName() == n) {
			room2.getSafe().deactivate();
			room2.setGuest(null);
			assert room2.getGuest() == null;
		}
		else { 
		} 
		
	}
	
	//Queries
	//Returns a free room
	public Room getFreeRoom() {
		if (room1.getGuest() == null) {
			assert room1.getGuest() == null;
			return room1;
		}
		else if (room2.getGuest() == null) {
			assert room2.getGuest() == null;
			return room2;
		}
		else {
			assert room1.getGuest() != null;
			assert room2.getGuest() != null;
			return null;
		}
	}
	
	//Returns the room of a guest
	public Room getRoom(String n) {
		if (room1.getGuest() != null && room1.getGuest().getName() == n) {
			assert room1.getGuest().getName() == n;
			return room1;
		}
		else if (room2.getGuest() != null && room2.getGuest().getName() == n){
			assert room2.getGuest().getName() == n;
			return room2;
		}
		else { 
			return null;
		}

	}
	
	//Returns the password of all rooms
	public Password getPassword() {
		return room1.getSafe().getPassword();
		
	}
	
	//Returns the name of guest in room 1
	public String toString() {
		assert room1.getGuest() != null;
		return room1.getGuest().getName();
	}

}                                                                                                                                                                                 
