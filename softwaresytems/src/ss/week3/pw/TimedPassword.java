package ss.week3.pw;

import java.lang.System;

public class TimedPassword extends Password {
	
	private static final long INITIAL = 1000000000;
	public long validTime;
	public long time;
	
	public TimedPassword(long a) {
		validTime = a;
		time = System.currentTimeMillis();
		
	}
	public TimedPassword() {
		validTime = INITIAL;
		time = System.currentTimeMillis();
	}
	
	public boolean isExpired() {
			return ((System.currentTimeMillis()) - this.time) > this.validTime;		
	}
	
	public boolean setWord(String oldpass, String newpass) {
		this.time = System.currentTimeMillis();
		return super.setWord(oldpass, newpass);
	}
		
	
	
	//voor test
	public long getTime(){
		return this.time;
	}
	
	public long getTotalTime(){
		return System.currentTimeMillis() - this.time;
	}
	
	public long getValidTime() {
		return this.validTime;
		
	}
	
	//test
	public static void main(String[] args) throws Exception {
		TimedPassword a = new TimedPassword(2000);
		Thread.sleep(2000);
		a.setWord(BasicChecker.INITPASS, "test123");
		System.out.println("time= " + a.getTime() + "totaltime= " +  a.getTotalTime() + "validtime= " + a.getValidTime() + a.isExpired());
		
	}
}
