package ss.week3.pw;

public class BasicChecker implements Checker {
	
	public static final String INITPASS = "wachtwoord";

	@Override
	public boolean acceptable(String password) {
		return (password.length() > 5 && !password.contains(" "));
	}
	
	@Override
	public String generatePassword() {
		return INITPASS;
	}

}
