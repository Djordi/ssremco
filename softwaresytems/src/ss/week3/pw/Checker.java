/**
 * 
 */
package ss.week3.pw;

/**
 * @author Remco Loof
 *
 */
public interface Checker {
	
	boolean acceptable(String password);
	String generatePassword();
	

}
