package ss.week3.pw;

public class StrongChecker implements Checker {
	
	public static final String INITPASS = "wachtwoord1";

	public boolean acceptable(String password) {
		Checker a = new BasicChecker();
		return (Character.isAlphabetic(password.charAt(0))) && (Character.isDigit(password.charAt((password.length()-1)))) && (a.acceptable(password));
	}
	
	public String generatePassword(){
		return INITPASS;
	}
	
	//test
	public static void main(String[] args) {
		Checker p = new StrongChecker();
		System.out.println(p.acceptable("kajdsfkl"));
	}
}
