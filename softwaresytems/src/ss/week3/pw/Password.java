package ss.week3.pw;

import ss.week3.pw.BasicChecker;
import ss.week3.pw.Checker;

public class Password {
	
	public String password;
	public static final String INITIAL = "wachtwoord"; 
	public Checker checker;
	public String factoryPassword;
	public long time;
	
	public Password(Checker c){
		password = INITIAL;
		factoryPassword = "wachtwoord1";
		checker = c;
	}
	
	public Password() {
		new Password(new BasicChecker());	
	}
	
	public Checker getChecker() {
		return this.checker;
	}
	
	public String getFactoryPassword() {
		return this.factoryPassword;
	}
		
	public boolean acceptable(String suggestion) {
		if (suggestion.length() < 6 || suggestion.contains(" ")) {
			return false;
		}
		else {
			return true;
		}
		
	}
	
	public boolean testWord(String test) {
		if (test.equals(password)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean setWord(String oldpass, String newpass) {
		if (testWord(oldpass) && acceptable(newpass)) {
			password = newpass;
			return true;
		}
		else {
			return false;
		}
		
	}

}
